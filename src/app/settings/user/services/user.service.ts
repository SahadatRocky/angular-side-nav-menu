import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { ErrorHandlerService } from "../../../core/services/error-handler.service";
import { baseUrl } from "../../../../environments/environment";
import { Observable, catchError } from "rxjs";
import { Users } from "../models/users.model";
import { User } from "../models/user.model";


@Injectable({
    providedIn: 'root'
})
export class UserService{
     
   resourceUrl !: string;
   constructor(private http: HttpClient,
    private readonly errorHandlerService: ErrorHandlerService){
        this.resourceUrl = `${baseUrl}/user`;
   } 


   getUserList(pageIndex: number, pageSize: number): Observable<Users> {
    return this.http.get<Users>(`${this.resourceUrl}`, {
      params: {
        page: pageIndex,
        size: pageSize
      }
    }).pipe(
      catchError(this.errorHandlerService.handleError)
    )
  }

  getUser(userId: string): Observable<User> {
    return this.http.get<User>(`${this.resourceUrl}/${userId}`).pipe(
      // tap(response => console.log(response)),
      catchError(this.errorHandlerService.handleError)
    )
  }

  getLoggedInUser(): Observable<User> {
    return this.http.get<User>(`${this.resourceUrl}/logged-in-user-info`).pipe(
      // tap(response => console.log(response)),
      catchError(this.errorHandlerService.handleError)
    )
  }

  createUser(user: User): Observable<User> {
    return this.http.post<User>(this.resourceUrl, user).pipe(
      // tap(response => console.log(response)),
      catchError(this.errorHandlerService.handleError)
    )
  }

  updateUser(userId: string, user: User): Observable<User> {
    return this.http.put<User>(`${this.resourceUrl}/${userId}`, user).pipe(
      // tap(response => console.log(response)),
      catchError(this.errorHandlerService.handleError)
    )
  }

  getParams(myMap : Map<string, any>): HttpParams{
    let params = new HttpParams();
    myMap.forEach((value : any, key: any)=> {
        params = params.append(key, value);
    }) 
    return params;
  }

  userFilterRequest(pageIndex: number, pageSize: number, myMap : Map<string, any>): Observable<Users>{
    
    myMap.set('page', pageIndex);
    myMap.set('size', pageSize);
    let params = this.getParams(myMap);
    return this.http.get<Users>(`${this.resourceUrl}/search`, {params})
        .pipe(
            catchError(this.errorHandlerService.handleError)
        );
  }

}