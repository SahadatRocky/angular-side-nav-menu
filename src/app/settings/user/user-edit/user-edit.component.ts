import { Location } from '@angular/common';
import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { User } from '../models/user.model';
import { ReviewType } from '../../../core/models/review-type.model';
import { Observable, debounceTime, map, startWith } from 'rxjs';
import { autocompleteValidator } from '../../../core/validators/autocomplete.validator';


@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.scss']
})
export class UserEditComponent implements OnChanges {

  form!: FormGroup;

  typeGroup = new FormGroup({
    approverTypeId: new FormControl('')
  })

  @Input() userId!: string;
  @Input() user!: User | null;
  @Input() reviewTypeList!: ReviewType[];

  @Output() onCreate = new EventEmitter<User>();
  @Output() onUpdate = new EventEmitter<User>();
  filteredReviewTypeList!: Observable<ReviewType[]>;

  constructor(
    private fb: FormBuilder
    , private readonly location: Location
  ) { }

  ngOnChanges(changes: SimpleChanges): void {

    this.initForm();

    if (this.reviewTypeList?.length > 0) {
      this.reviewType.setValidators([Validators.required, autocompleteValidator(this.reviewTypeList)])
      this.setReviewTypeList()
    }

    if (this.user) {
      this.form.patchValue({ ...this.user });
      this.emailAddress.disable();
    }
  }

  initForm() {

    if (this.form) return;
    this.form = this.fb.group({
      userName: ['', [Validators.required]],
      emailAddress: ['', [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]],
      mobileNumber: ['', [Validators.required, Validators.pattern('[0-9]+')]],
      jobTitle:['',[Validators.required]],
      position: ['',[Validators.required]],
      password:['',[Validators.required]],
      reviewType:[''],
      isActive: ['', []],
    });
  }

  setReviewTypeList() {
    if (this.reviewType) {
      this.filteredReviewTypeList = this.reviewType.valueChanges.pipe(
        startWith(''),
        debounceTime(200),
        map(value =>
          this.reviewTypeList?.filter((option: ReviewType) => option?.reviewTypeName?.toLowerCase().includes(value.toString().toLowerCase())) ?? ''
        ));
    }
  }


  public isValidNumber(event: any): boolean {
    const charCode = event.keyCode;
    if (charCode == 8 || charCode == 46 || (charCode >= 48 && charCode <= 57) || (charCode >= 96 && charCode <= 105)) {
      return true;
    }
    return false;
  }

  displayReviewTypeAutocompleteValue(e: any): string {
    return e ? e.reviewTypeName : '';
  }

  back() {
    this.location.back();
  }

  submit() {
    const data: User = {
      ...this.form.value
      , reviewTypeId: this.reviewType?.value['id'] ?? ''
      , status: 'true'
      , version: 0
    }

    this.onCreate.emit(data)
  }

  edit() {
    const data: User = {
      ...this.user
      , ...this.form.value
      , reviewTypeId: this.reviewType?.value['id'] ?? ''
    }

    this.onUpdate.emit(data)
  }


  get userName() {
    return this.form.controls['userName'];
  }
  get emailAddress() {
    return this.form.controls['emailAddress'];
  }

  get mobileNumber() {
    return this.form.controls['mobileNumber'];
  }

  get jobTitle(){
    return this.form.controls['jobTitle']; 
  }

  get position(){
    return this.form.controls['position']; 
  }

  get password(){
    return this.form.controls['password']; 
  }

  get reviewType() {
    return this.form.controls['reviewType'];
  }

  get isActive() {
    return this.form.controls['isActive'];
  }


}

