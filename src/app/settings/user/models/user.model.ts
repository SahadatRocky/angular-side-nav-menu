

export interface User{
    name : string;
    mobileNumber : string;
    email : string;
    status : boolean; 
}