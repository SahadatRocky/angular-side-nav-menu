import { User } from "./user.model";

export interface Users{
    content: User[];
    totalElements: number;
    size: number;
}