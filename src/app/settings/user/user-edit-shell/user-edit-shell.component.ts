import { Component, Input, OnChanges, OnInit, SimpleChanges, signal } from '@angular/core';
import { User } from '../models/user.model';
import { Router } from '@angular/router';
import { UserService } from '../services/user.service';
import { MatDialog } from '@angular/material/dialog';
import { LoadingService } from '../../../core/services/loading.service';
import { ReviewType } from '../../../core/models/review-type.model';

@Component({
  selector: 'app-user-edit-shell',
  templateUrl: './user-edit-shell.component.html',
  styleUrls: ['./user-edit-shell.component.scss']
})
export class UserEditShellComponent implements OnInit, OnChanges {

  @Input() id!: string;

  user = signal<User | null>(null)
  reviewTypeList = signal<ReviewType[]>([
    {
     id : '1', 
     reviewTypeName: 'A ROLE'
    },
    {
      id : '2', 
      reviewTypeName: 'B ROLE'
     }
  ]);
  
  constructor(
    private readonly userService: UserService
    , private readonly loadingService: LoadingService
    , private readonly router: Router
    , private readonly dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.getUser()
    this.getAllReviewType()
  }

  ngOnChanges(changes: SimpleChanges): void { }

  getUser() {
    if (this.id && this.id != '0') {
      this.userService.getUser(this.id).subscribe({
        next: user => this.user.set(user),
        error: error => { }
      })
    }
  }

  getAllReviewType(){
    // this.reviewTypeService.getAllReviewType().subscribe({
    //   next: branchList => this.reviewTypeList.set(branchList),
    //   error: error => this.reviewTypeList.set([])
    // })
  }

  
  createUser(user: User) {
    this.loadingService.showLoaderUntilCompleted(
      this.userService.createUser(user)
    ).subscribe({
      next: user => {
        // this.messageService.showSuccessMessage('User Created Successfully');
        this.router.navigateByUrl(`/user`, { replaceUrl: true })
      },
      error: error => {
        console.log(error)
        // this.messageService.showErrorMessage('Failed To Create User')
      }
    })
  }

  updateUser(user: User) {
    this.loadingService.showLoaderUntilCompleted(
      this.userService.updateUser(this.id, user)
    ).subscribe({
      next: user => {
        // this.messageService.showSuccessMessage('User Updated Successfully');
        this.user.set(user)
        // this.router.navigateByUrl(`/user/${user.id}/edit`)
      },
      error: error => {
        console.log(error)
        // this.messageService.showErrorMessage('Failed To Update User')
      }
    })
  }
}
