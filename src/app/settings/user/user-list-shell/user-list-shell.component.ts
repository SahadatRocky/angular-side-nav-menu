import { Component, OnInit, signal } from '@angular/core';
import { User } from '../models/user.model';
import { UserService } from '../services/user.service';
import { LoadingService } from '../../../core/services/loading.service';
import { PageChangeInfo } from '../../../core/models/pageChangeInfo';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-list-shell',
  templateUrl: './user-list-shell.component.html',
  styleUrl: './user-list-shell.component.css'
})
export class UserListShellComponent implements OnInit {
     
  userList = signal<User[]>([]);
  pageSize = signal<number>(0);
  totalElements = signal<number>(0);
  constructor(
    private readonly userService: UserService
    , private readonly loadingService: LoadingService
    , private router : Router){}

  ngOnInit(): void {
    this.getUserList();
  }

  getUserList(pageIndex = 0, pageSize = 10){
      this.userService.getUserList(pageIndex, pageSize).subscribe({
        next: users => {
          this.userList.set(users.content);
          this.totalElements.set(users.totalElements);
          this.pageSize.set(users.size);
        },
        error: error => {
          this.userList.set([]);
          this.totalElements.set(0);
          this.pageSize.set(0);
        }
      }
      );
  }

  onPageChanged(pageChangeInfo: PageChangeInfo): void {
    this.getUserList(pageChangeInfo.pageIndex, pageChangeInfo.pageSize)
  }

  clickedButton(id: string) {
    // console.log(id);
    this.router.navigateByUrl(`/settings/user/${id}/edit`)
  }

}
