import { Component, OnInit, signal } from '@angular/core';
import { LoadingService } from '../../../core/services/loading.service';
import { PageChangeInfo } from '../../../core/models/pageChangeInfo';
import { Router } from '@angular/router';
import { DepartmentService } from '../services/department.service';
import { Department } from '../models/department.model';

@Component({
  selector: 'app-department-list-shell',
  templateUrl: './department-list-shell.component.html',
  styleUrl: './department-list-shell.component.css'
})
export class DepartmentListShellComponent implements OnInit {
     
  departmentList = signal<Department[]>([]);
  pageSize = signal<number>(0);
  totalElements = signal<number>(0);
  constructor(
    private readonly departmentService: DepartmentService
    , private readonly loadingService: LoadingService
    , private router : Router){}

  ngOnInit(): void {
    this.getDepartmentList();
  }

  getDepartmentList(pageIndex = 0, pageSize = 10){
      this.departmentService.getDepartmentList(pageIndex, pageSize).subscribe({
        next: departments => {
          this.departmentList.set(departments.content);
          this.totalElements.set(departments.totalElements);
          this.pageSize.set(departments.size);
        },
        error: error => {
          this.departmentList.set([]);
          this.totalElements.set(0);
          this.pageSize.set(0);
        }
      }
      );
  }

  onPageChanged(pageChangeInfo: PageChangeInfo): void {
    this.getDepartmentList(pageChangeInfo.pageIndex, pageChangeInfo.pageSize)
  }

  clickedButton(id: string) {
    // console.log(id);
    this.router.navigateByUrl(`/settings/department/${id}/edit`)
  }

}
