import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DepartmentListShellComponent } from './department-list-shell.component';

describe('DepartmentListShellComponent', () => {
  let component: DepartmentListShellComponent;
  let fixture: ComponentFixture<DepartmentListShellComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DepartmentListShellComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(DepartmentListShellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
