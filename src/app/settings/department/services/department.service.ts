import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { ErrorHandlerService } from "../../../core/services/error-handler.service";
import { baseUrl } from "../../../../environments/environment";
import { Observable, catchError } from "rxjs";
import { Departments } from "../models/departments.model";
import { Department } from "../models/department.model";


@Injectable({
    providedIn: 'root'
})
export class DepartmentService{
     
   resourceUrl !: string;
   constructor(private http: HttpClient,
    private readonly errorHandlerService: ErrorHandlerService){
        this.resourceUrl = `${baseUrl}/department`;
   }

   getDepartmentList(pageIndex: number, pageSize: number): Observable<Departments> {
    return this.http.get<Departments>(`${this.resourceUrl}`, {
      params: {
        page: pageIndex,
        size: pageSize
      }
    }).pipe(
      catchError(this.errorHandlerService.handleError)
    )
  }

  getDepartment(departmentId: string): Observable<Department> {
    return this.http.get<Department>(`${this.resourceUrl}/${departmentId}`).pipe(
      // tap(response => console.log(response)),
      catchError(this.errorHandlerService.handleError)
    )
  }


  createDepartment(department: Department): Observable<Department> {
    return this.http.post<Department>(this.resourceUrl, department).pipe(
      // tap(response => console.log(response)),
      catchError(this.errorHandlerService.handleError)
    )
  }

  updateDepartment(departmentId: string, department: Department): Observable<Department> {
    return this.http.put<Department>(`${this.resourceUrl}/${departmentId}`, department).pipe(
      // tap(response => console.log(response)),
      catchError(this.errorHandlerService.handleError)
    )
  }

  getParams(myMap : Map<string, any>): HttpParams{
    let params = new HttpParams();
    myMap.forEach((value : any, key: any)=> {
        params = params.append(key, value);
    }) 
    return params;
  }

  departmentFilterRequest(pageIndex: number, pageSize: number, myMap : Map<string, any>): Observable<Departments>{
    
    myMap.set('page', pageIndex);
    myMap.set('size', pageSize);
    let params = this.getParams(myMap);
    return this.http.get<Departments>(`${this.resourceUrl}/search`, {params})
        .pipe(
            catchError(this.errorHandlerService.handleError)
        );
  }

}