

export interface Department{
    name : string;
    description : string;
    deviceType : string;
    code: string;
    status : boolean; 
}