import { Department } from "./department.model";

export interface Departments{
    content: Department[];
    totalElements: number;
    size: number;
}