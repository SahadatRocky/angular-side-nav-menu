import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DepartmentEditShellComponent } from './department-edit-shell.component';

describe('DepartmentEditShellComponent', () => {
  let component: DepartmentEditShellComponent;
  let fixture: ComponentFixture<DepartmentEditShellComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DepartmentEditShellComponent]
    });
    fixture = TestBed.createComponent(DepartmentEditShellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
