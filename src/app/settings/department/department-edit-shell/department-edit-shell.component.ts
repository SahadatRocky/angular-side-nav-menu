import { Component, Input, OnChanges, OnInit, SimpleChanges, signal } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { LoadingService } from '../../../core/services/loading.service';
import { DepartmentService } from '../services/department.service';
import { Department } from '../models/department.model';
import { NewDepartmentType } from '../../../core/models/new-department-type.model';

@Component({
  selector: 'app-department-edit-shell',
  templateUrl: './department-edit-shell.component.html',
  styleUrls: ['./department-edit-shell.component.scss']
})
export class DepartmentEditShellComponent implements OnInit, OnChanges {

  @Input() id!: string;

  department = signal<Department | null>(null)
  newDepartmentList = signal<NewDepartmentType[]>([
    {
     id : '1', 
     name: 'A Dept'
    },
    {
      id : '2', 
      name: 'B Dept'
     }
  ]);
  
  constructor(
    private readonly departmentService: DepartmentService
    , private readonly loadingService: LoadingService
    , private readonly router: Router
    , private readonly dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.getDepartment()
    this.getAllNewDepartment()
  }

  ngOnChanges(changes: SimpleChanges): void { }

  getDepartment() {
    if (this.id && this.id != '0') {
      this.departmentService.getDepartment(this.id).subscribe({
        next: department => this.department.set(department),
        error: error => { }
      })
    }
  }

  getAllNewDepartment(){
    // this.reviewTypeService.getAllReviewType().subscribe({
    //   next: branchList => this.reviewTypeList.set(branchList),
    //   error: error => this.reviewTypeList.set([])
    // })
  }

  
  createDepartment(department: Department) {
    this.loadingService.showLoaderUntilCompleted(
      this.departmentService.createDepartment(department)
    ).subscribe({
      next: department => {
        // this.messageService.showSuccessMessage('User Created Successfully');
        this.router.navigateByUrl(`/department`, { replaceUrl: true })
      },
      error: error => {
        console.log(error)
        // this.messageService.showErrorMessage('Failed To Create User')
      }
    })
  }

  updateDepartment(department: Department) {
    this.loadingService.showLoaderUntilCompleted(
      this.departmentService.updateDepartment(this.id, department)
    ).subscribe({
      next: department => {
        // this.messageService.showSuccessMessage('User Updated Successfully');
        this.department.set(department)
        // this.router.navigateByUrl(`/user/${user.id}/edit`)
      },
      error: error => {
        console.log(error)
        // this.messageService.showErrorMessage('Failed To Update User')
      }
    })
  }
}
