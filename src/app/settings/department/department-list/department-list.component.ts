import { AfterViewInit, Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { PageChangeInfo } from '../../../core/models/pageChangeInfo';
import { MatPaginator } from '@angular/material/paginator';
import { Subscription, tap } from 'rxjs';
import { ColumnInfo } from '../../../core/models/column-info.model';
import { Department } from '../models/department.model';

@Component({
  selector: 'app-department-list',
  templateUrl: './department-list.component.html',
  styleUrl: './department-list.component.css'
})
export class DepartmentListComponent implements OnInit, OnChanges , AfterViewInit, OnDestroy{

  @Input() departmentList!: Department[];
  @Input() pageSize!: number;
  @Input() totalElements!: number;
  @Output() pageChanged = new EventEmitter<PageChangeInfo>();
  @Output() onButtonClicked = new EventEmitter<string>();
  @ViewChild(MatPaginator) paginator !: MatPaginator;
  sub !: Subscription;
  tableHeaderCss = 'text-base text-black-500 font-bold'
  constructor(){}

  ngOnInit(){

  }

  ngOnChanges(changes: SimpleChanges): void {

  }

  ngAfterViewInit(): void {
    this.sub = this.paginator.page
    .pipe(
      tap(
        ()=> this.pageChanged.emit({
            pageIndex: this.paginator.pageIndex,
            pageSize: this.paginator.pageSize
        })))
      .subscribe()
  }

  ngOnDestroy(){
    if(this.sub) this.sub.unsubscribe();
  }

  getDisplayedColumns(): string[] {
    return this.getColumnInfos().map(item => item.columnDef);
  }

  getColumnInfos(): ColumnInfo[] {
    return [
      {
        columnDef: 'name',
        header: 'Name'
      },
      {
        columnDef: 'description',
        header: 'Description'
      },
      {
        columnDef: 'code',
        header: 'Code'
      },
      {
        columnDef: 'status',
        header: 'Status'
      },
      {
        columnDef: 'action',
        header: 'Action'
      }
    ];
  }

  getRowData(columnInfo: ColumnInfo, row:any): string {

    if(columnInfo.columnDef == 'status') {
      return row.isActive ? 'Active' : 'Inactive'
    }
    
    return row[columnInfo.columnDef];
  }

  clickedButton(id:string) {
    this.onButtonClicked.emit(id)
  }

}

