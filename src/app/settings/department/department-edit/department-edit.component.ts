import { Location } from '@angular/common';
import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable, debounceTime, map, startWith } from 'rxjs';
import { autocompleteValidator } from '../../../core/validators/autocomplete.validator';
import { Department } from '../models/department.model';
import { NewDepartmentType } from '../../../core/models/new-department-type.model';


@Component({
  selector: 'app-department-edit',
  templateUrl: './department-edit.component.html',
  styleUrls: ['./department-edit.component.scss']
})
export class DepartmentEditComponent implements OnChanges {

  form!: FormGroup;

  typeGroup = new FormGroup({
    approverTypeId: new FormControl('')
  })

  @Input() departmentId!: string;
  @Input() department!: Department | null;
  @Input() newDepartmentList!: NewDepartmentType[];

  @Output() onCreate = new EventEmitter<Department>();
  @Output() onUpdate = new EventEmitter<Department>();
  filtereddDepartmentTypeList!: Observable<NewDepartmentType[]>;

  constructor(
    private fb: FormBuilder
    , private readonly location: Location
  ) { }

  ngOnChanges(changes: SimpleChanges): void {

    this.initForm();

    if (this.newDepartmentList?.length > 0) {
      this.newDepartment.setValidators([Validators.required, autocompleteValidator(this.newDepartmentList)])
      this.setNewDepartmentList()
    }

    if (this.department) {
      this.form.patchValue({ ...this.department });
    }
  }

  initForm() {

    if (this.form) return;
    this.form = this.fb.group({
      newDepartment: ['',[Validators.required]],
      name: ['', [Validators.required]],
      description: ['', [Validators.required]],
      code:[''],
      isActive: ['', []],
    });
  }

  setNewDepartmentList() {
    if (this.newDepartment) {
      this.filtereddDepartmentTypeList = this.newDepartment.valueChanges.pipe(
        startWith(''),
        debounceTime(200),
        map(value =>
          this.newDepartmentList?.filter((option: NewDepartmentType) => option?.name?.toLowerCase().includes(value.toString().toLowerCase())) ?? ''
        ));
    }
  }


  displayDepartmentTypeAutocompleteValue(e: any): string {
    return e ? e.name : '';
  }

  back() {
    this.location.back();
  }

  submit() {
    const data: Department = {
      ...this.form.value
      , newDepartmentId: this.newDepartment?.value['id'] ?? ''
      , status: 'true'
      , version: 0
    }

    this.onCreate.emit(data)
  }

  edit() {
    const data: Department = {
      ...this.department
      , ...this.form.value
      , newDepartmentId: this.newDepartment?.value['id'] ?? ''
    }

    this.onUpdate.emit(data)
  }


  get newDepartment() {
    return this.form.controls['newDepartment'];
  }
  get name() {
    return this.form.controls['name'];
  }

  get description() {
    return this.form.controls['description'];
  }

  get code(){
    return this.form.controls['code'];
  }

  get isActive() {
    return this.form.controls['isActive'];
  }


}

