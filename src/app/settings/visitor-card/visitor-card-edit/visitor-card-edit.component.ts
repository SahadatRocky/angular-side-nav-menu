import { Location } from '@angular/common';
import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { VisitorCard } from '../models/visitor-card.model';


@Component({
  selector: 'app-visitor-card-edit',
  templateUrl: './visitor-card-edit.component.html',
  styleUrls: ['./visitor-card-edit.component.scss']
})
export class VisitorCardEditComponent implements OnChanges {

  form!: FormGroup;

  typeGroup = new FormGroup({
    approverTypeId: new FormControl('')
  })

  @Input() visitorCardId!: string;
  @Input() visitorCard!: VisitorCard | null;

  @Output() onCreate = new EventEmitter<VisitorCard>();
  @Output() onUpdate = new EventEmitter<VisitorCard>();

  constructor(
    private fb: FormBuilder
    , private readonly location: Location
  ) { }

  ngOnChanges(changes: SimpleChanges): void {

    this.initForm();

    if (this.visitorCard) {
      this.form.patchValue({ ...this.visitorCard });
    }
  }

  initForm() {
    if (this.form) return;
    this.form = this.fb.group({
      cardNumber: ['', [Validators.required]],
      cardDescription:['', [Validators.required]],
      isActive: ['', []],
    });
  }

 
  back() {
    this.location.back();
  }

  submit() {
    const data: VisitorCard = {
      ...this.form.value
      , status: 'true'
      , version: 0
    }

    this.onCreate.emit(data)
  }

  edit() {
    const data: VisitorCard = {
      ...this.visitorCard
      , ...this.form.value
    }

    this.onUpdate.emit(data)
  }

  get cardNumber() {
    return this.form.controls['cardNumber'];
  }

  get cardDescription() {
    return this.form.controls['cardDescription'];
  }

  get isActive() {
    return this.form.controls['isActive'];
  }

}

