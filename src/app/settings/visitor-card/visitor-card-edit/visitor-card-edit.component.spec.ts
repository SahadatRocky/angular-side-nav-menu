import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VisitorCardEditComponent } from './visitor-card-edit.component';

describe('VisitorCardEditComponent', () => {
  let component: VisitorCardEditComponent;
  let fixture: ComponentFixture<VisitorCardEditComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [VisitorCardEditComponent]
    });
    fixture = TestBed.createComponent(VisitorCardEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
