import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VisitorCardListShellComponent } from './visitor-card-list-shell.component';

describe('VisitorCardListShellComponent', () => {
  let component: VisitorCardListShellComponent;
  let fixture: ComponentFixture<VisitorCardListShellComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [VisitorCardListShellComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(VisitorCardListShellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
