import { Component, OnInit, signal } from '@angular/core';
import { LoadingService } from '../../../core/services/loading.service';
import { PageChangeInfo } from '../../../core/models/pageChangeInfo';
import { Router } from '@angular/router';
import { VisitorCard } from '../models/visitor-card.model';
import { VisitorCardService } from '../services/visitor-card.service';

@Component({
  selector: 'app-visitor-card-list-shell',
  templateUrl: './visitor-card-list-shell.component.html',
  styleUrl: './visitor-card-list-shell.component.css'
})
export class VisitorCardListShellComponent implements OnInit {
     
  visitorCardList = signal<VisitorCard[]>([]);
  pageSize = signal<number>(0);
  totalElements = signal<number>(0);
  constructor(
    private readonly visitorCardService: VisitorCardService
    , private readonly loadingService: LoadingService
    , private router : Router){}

  ngOnInit(): void {
    this.getVisitorCardList();
  }

  getVisitorCardList(pageIndex = 0, pageSize = 10){
      this.visitorCardService.getVisitorCardList(pageIndex, pageSize).subscribe({
        next: vsitorCards => {
          this.visitorCardList.set(vsitorCards.content);
          this.totalElements.set(vsitorCards.totalElements);
          this.pageSize.set(vsitorCards.size);
        },
        error: error => {
          this.visitorCardList.set([]);
          this.totalElements.set(0);
          this.pageSize.set(0);
        }
      }
    );
  }

  onPageChanged(pageChangeInfo: PageChangeInfo): void {
    this.getVisitorCardList(pageChangeInfo.pageIndex, pageChangeInfo.pageSize)
  }

  clickedButton(id: string) {
    // console.log(id);
    this.router.navigateByUrl(`/settings/visitor-card/${id}/edit`)
  }

}
