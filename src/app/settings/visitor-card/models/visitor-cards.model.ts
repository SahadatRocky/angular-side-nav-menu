import { VisitorCard } from "./visitor-card.model";

export interface VisitorCards{
    content: VisitorCard[];
    totalElements: number;
    size: number;
}