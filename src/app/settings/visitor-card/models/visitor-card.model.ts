

export interface VisitorCard{
    cardNo : string;
    description : string;
    status : boolean;
}