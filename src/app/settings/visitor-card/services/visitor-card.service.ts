import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { ErrorHandlerService } from "../../../core/services/error-handler.service";
import { baseUrl } from "../../../../environments/environment";
import { Observable, catchError } from "rxjs";
import { VisitorCards } from "../models/visitor-cards.model";
import { VisitorCard } from "../models/visitor-card.model";


@Injectable({
    providedIn: 'root'
})
export class VisitorCardService{
     
   resourceUrl !: string;
   constructor(private http: HttpClient,
    private readonly errorHandlerService: ErrorHandlerService){
        this.resourceUrl = `${baseUrl}/visitor-card`;
   } 


   getVisitorCardList(pageIndex: number, pageSize: number): Observable<VisitorCards> {
    return this.http.get<VisitorCards>(`${this.resourceUrl}`, {
      params: {
        page: pageIndex,
        size: pageSize
      }
    }).pipe(
      catchError(this.errorHandlerService.handleError)
    )
  }

  getVisitorCard(visitorCardId: string): Observable<VisitorCard> {
    return this.http.get<VisitorCard>(`${this.resourceUrl}/${visitorCardId}`).pipe(
      // tap(response => console.log(response)),
      catchError(this.errorHandlerService.handleError)
    )
  }


  createVisitorCard(visitorCard : VisitorCard): Observable<VisitorCard> {
    return this.http.post<VisitorCard>(this.resourceUrl, visitorCard).pipe(
      // tap(response => console.log(response)),
      catchError(this.errorHandlerService.handleError)
    )
  }

  updateVisitorCard(visitorCardId: string, visitorCard: VisitorCard): Observable<VisitorCard> {
    return this.http.put<VisitorCard>(`${this.resourceUrl}/${visitorCardId}`, visitorCard).pipe(
      // tap(response => console.log(response)),
      catchError(this.errorHandlerService.handleError)
    )
  }

  getParams(myMap : Map<string, any>): HttpParams{
    let params = new HttpParams();
    myMap.forEach((value : any, key: any)=> {
        params = params.append(key, value);
    }) 
    return params;
  }

  visitorCardFilterRequest(pageIndex: number, pageSize: number, myMap : Map<string, any>): Observable<VisitorCards>{
    
    myMap.set('page', pageIndex);
    myMap.set('size', pageSize);
    let params = this.getParams(myMap);
    return this.http.get<VisitorCards>(`${this.resourceUrl}/search`, {params})
        .pipe(
            catchError(this.errorHandlerService.handleError)
        );
  }

}