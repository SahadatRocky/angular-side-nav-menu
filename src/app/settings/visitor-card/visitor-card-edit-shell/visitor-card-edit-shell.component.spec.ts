import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VisitorCardEditShellComponent } from './visitor-card-edit-shell.component';

describe('VisitorCardEditShellComponent', () => {
  let component: VisitorCardEditShellComponent;
  let fixture: ComponentFixture<VisitorCardEditShellComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [VisitorCardEditShellComponent]
    });
    fixture = TestBed.createComponent(VisitorCardEditShellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
