import { Component, Input, OnChanges, OnInit, SimpleChanges, signal } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { LoadingService } from '../../../core/services/loading.service';
import { VisitorCard } from '../models/visitor-card.model';
import { VisitorCardService } from '../services/visitor-card.service';

@Component({
  selector: 'app-visitor-card-edit-shell',
  templateUrl: './visitor-card-edit-shell.component.html',
  styleUrls: ['./visitor-card-edit-shell.component.scss']
})
export class VisitorCardEditShellComponent implements OnInit, OnChanges {

  @Input() id!: string;

  visitorCard = signal<VisitorCard | null>(null);

  constructor(
    private readonly visitorCardService: VisitorCardService
    , private readonly loadingService: LoadingService
    , private readonly router: Router
    , private readonly dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.getVisitorCard()
  }

  ngOnChanges(changes: SimpleChanges): void { }

  getVisitorCard() {
    if (this.id && this.id != '0') {
      this.visitorCardService.getVisitorCard(this.id).subscribe({
        next: visitorCard => this.visitorCard.set(visitorCard),
        error: error => { }
      })
    }
  }

  createVisitorCard(visitorCard: VisitorCard) {
    this.loadingService.showLoaderUntilCompleted(
      this.visitorCardService.createVisitorCard(visitorCard)
    ).subscribe({
      next: visitorCard => {
        // this.messageService.showSuccessMessage('User Created Successfully');
        this.router.navigateByUrl(`/visitor-card`, { replaceUrl: true })
      },
      error: error => {
        console.log(error)
        // this.messageService.showErrorMessage('Failed To Create User')
      }
    })
  }

  updateVisitorCard(visitorCard: VisitorCard) {
    this.loadingService.showLoaderUntilCompleted(
      this.visitorCardService.updateVisitorCard(this.id, visitorCard)
    ).subscribe({
      next: visitorCard => {
        // this.messageService.showSuccessMessage('User Updated Successfully');
        this.visitorCard.set(visitorCard)
        // this.router.navigateByUrl(`/user/${user.id}/edit`)
      },
      error: error => {
        console.log(error)
        // this.messageService.showErrorMessage('Failed To Update User')
      }
    })
  }
}
