import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VisitorCardListComponent } from './visitor-card-list.component';

describe('VisitorCardListComponent', () => {
  let component: VisitorCardListComponent;
  let fixture: ComponentFixture<VisitorCardListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [VisitorCardListComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(VisitorCardListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
