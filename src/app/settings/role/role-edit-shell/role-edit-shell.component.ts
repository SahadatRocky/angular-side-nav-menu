import { Component, Input, OnChanges, OnInit, SimpleChanges, signal } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { LoadingService } from '../../../core/services/loading.service';
import { Role } from '../models/role.model';
import { RoleService } from '../services/role.service';

@Component({
  selector: 'app-role-edit-shell',
  templateUrl: './role-edit-shell.component.html',
  styleUrls: ['./role-edit-shell.component.scss']
})
export class RoleEditShellComponent implements OnInit, OnChanges {

  @Input() id!: string;

  role = signal<Role | null>(null);

  constructor(
    private readonly roleService: RoleService
    , private readonly loadingService: LoadingService
    , private readonly router: Router
    , private readonly dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.getRole()
  }

  ngOnChanges(changes: SimpleChanges): void { }

  getRole() {
    if (this.id && this.id != '0') {
      this.roleService.getRole(this.id).subscribe({
        next: role => this.role.set(role),
        error: error => { }
      })
    }
  }

  createRole(role: Role) {
    this.loadingService.showLoaderUntilCompleted(
      this.roleService.createRole(role)
    ).subscribe({
      next: role => {
        // this.messageService.showSuccessMessage('User Created Successfully');
        this.router.navigateByUrl(`/role`, { replaceUrl: true })
      },
      error: error => {
        console.log(error)
        // this.messageService.showErrorMessage('Failed To Create User')
      }
    })
  }

  updateRole(role: Role) {
    this.loadingService.showLoaderUntilCompleted(
      this.roleService.updateRole(this.id, role)
    ).subscribe({
      next: role => {
        // this.messageService.showSuccessMessage('User Updated Successfully');
        this.role.set(role)
        // this.router.navigateByUrl(`/user/${user.id}/edit`)
      },
      error: error => {
        console.log(error)
        // this.messageService.showErrorMessage('Failed To Update User')
      }
    })
  }
}
