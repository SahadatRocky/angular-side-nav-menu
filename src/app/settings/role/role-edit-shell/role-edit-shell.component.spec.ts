import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RoleEditShellComponent } from './role-edit-shell.component';

describe('RoleEditShellComponent', () => {
  let component: RoleEditShellComponent;
  let fixture: ComponentFixture<RoleEditShellComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [RoleEditShellComponent]
    });
    fixture = TestBed.createComponent(RoleEditShellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
