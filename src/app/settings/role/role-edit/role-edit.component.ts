import { Location } from '@angular/common';
import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Role } from '../models/role.model';


@Component({
  selector: 'app-role-edit',
  templateUrl: './role-edit.component.html',
  styleUrls: ['./role-edit.component.scss']
})
export class RoleEditComponent implements OnChanges {

  form!: FormGroup;

  typeGroup = new FormGroup({
    approverTypeId: new FormControl('')
  })

  @Input() roleId!: string;
  @Input() role!: Role | null;

  @Output() onCreate = new EventEmitter<Role>();
  @Output() onUpdate = new EventEmitter<Role>();

  constructor(
    private fb: FormBuilder
    , private readonly location: Location
  ) { }

  ngOnChanges(changes: SimpleChanges): void {

    this.initForm();

    if (this.role) {
      this.form.patchValue({ ...this.role });
    }
  }

  initForm() {
    if (this.form) return;
    this.form = this.fb.group({
      roleName: ['', [Validators.required]],
      roleDescription:['', [Validators.required]],
      isActive: ['', []],
    });
  }

 
  back() {
    this.location.back();
  }

  submit() {
    const data: Role = {
      ...this.form.value
      , status: 'true'
      , version: 0
    }

    this.onCreate.emit(data)
  }

  edit() {
    const data: Role = {
      ...this.role
      , ...this.form.value
    }

    this.onUpdate.emit(data)
  }

  get roleName() {
    return this.form.controls['roleName'];
  }

  get roleDescription() {
    return this.form.controls['roleDescription'];
  }

  get isActive() {
    return this.form.controls['isActive'];
  }

}

