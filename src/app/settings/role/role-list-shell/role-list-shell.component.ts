import { Component, OnInit, signal } from '@angular/core';
import { Role } from '../models/role.model';
import { RoleService } from '../services/role.service';
import { LoadingService } from '../../../core/services/loading.service';
import { PageChangeInfo } from '../../../core/models/pageChangeInfo';
import { Router } from '@angular/router';

@Component({
  selector: 'app-role-list-shell',
  templateUrl: './role-list-shell.component.html',
  styleUrl: './role-list-shell.component.css'
})
export class RoleListShellComponent implements OnInit {
     
  roleList = signal<Role[]>([]);
  pageSize = signal<number>(0);
  totalElements = signal<number>(0);
  constructor(
    private readonly roleService: RoleService
    , private readonly loadingService: LoadingService
    , private router : Router){}

  ngOnInit(): void {
    this.getRoleList();
  }

  getRoleList(pageIndex = 0, pageSize = 10){
      this.roleService.getRolesList(pageIndex, pageSize).subscribe({
        next: roles => {
          this.roleList.set(roles.content);
          this.totalElements.set(roles.totalElements);
          this.pageSize.set(roles.size);
        },
        error: error => {
          this.roleList.set([]);
          this.totalElements.set(0);
          this.pageSize.set(0);
        }
      }
      );
  }

  onPageChanged(pageChangeInfo: PageChangeInfo): void {
    this.getRoleList(pageChangeInfo.pageIndex, pageChangeInfo.pageSize)
  }

  clickedButton(id: string) {
    // console.log(id);
    this.router.navigateByUrl(`/settings/role/${id}/edit`)
  }

}
