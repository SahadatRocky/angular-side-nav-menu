import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RoleListShellComponent } from './role-list-shell.component';

describe('RoleListShellComponent', () => {
  let component: RoleListShellComponent;
  let fixture: ComponentFixture<RoleListShellComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RoleListShellComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(RoleListShellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
