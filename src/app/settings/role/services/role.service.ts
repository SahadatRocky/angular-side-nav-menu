import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { ErrorHandlerService } from "../../../core/services/error-handler.service";
import { baseUrl } from "../../../../environments/environment";
import { Observable, catchError } from "rxjs";
import { Roles } from "../models/roles.model";
import { Role } from "../models/role.model";


@Injectable({
    providedIn: 'root'
})
export class RoleService{
     
   resourceUrl !: string;
   constructor(private http: HttpClient,
    private readonly errorHandlerService: ErrorHandlerService){
        this.resourceUrl = `${baseUrl}/role`;
   } 


   getRolesList(pageIndex: number, pageSize: number): Observable<Roles> {
    return this.http.get<Roles>(`${this.resourceUrl}`, {
      params: {
        page: pageIndex,
        size: pageSize
      }
    }).pipe(
      catchError(this.errorHandlerService.handleError)
    )
  }

  getRole(userId: string): Observable<Role> {
    return this.http.get<Role>(`${this.resourceUrl}/${userId}`).pipe(
      // tap(response => console.log(response)),
      catchError(this.errorHandlerService.handleError)
    )
  }


  createRole(role: Role): Observable<Role> {
    return this.http.post<Role>(this.resourceUrl, role).pipe(
      // tap(response => console.log(response)),
      catchError(this.errorHandlerService.handleError)
    )
  }

  updateRole(roleId: string, role: Role): Observable<Role> {
    return this.http.put<Role>(`${this.resourceUrl}/${roleId}`, role).pipe(
      // tap(response => console.log(response)),
      catchError(this.errorHandlerService.handleError)
    )
  }

  getParams(myMap : Map<string, any>): HttpParams{
    let params = new HttpParams();
    myMap.forEach((value : any, key: any)=> {
        params = params.append(key, value);
    }) 
    return params;
  }

  roleFilterRequest(pageIndex: number, pageSize: number, myMap : Map<string, any>): Observable<Roles>{
    
    myMap.set('page', pageIndex);
    myMap.set('size', pageSize);
    let params = this.getParams(myMap);
    return this.http.get<Roles>(`${this.resourceUrl}/search`, {params})
        .pipe(
            catchError(this.errorHandlerService.handleError)
        );
  }

}