import { Role } from "./role.model";

export interface Roles{
    content: Role[];
    totalElements: number;
    size: number;
}