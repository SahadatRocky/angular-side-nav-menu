import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserListShellComponent } from './user/user-list-shell/user-list-shell.component';
import { UserEditShellComponent } from './user/user-edit-shell/user-edit-shell.component';
import { RoleListShellComponent } from './role/role-list-shell/role-list-shell.component';
import { RoleEditShellComponent } from './role/role-edit-shell/role-edit-shell.component';
import { VisitorCardListShellComponent } from './visitor-card/visitor-card-list-shell/visitor-card-list-shell.component';
import { VisitorCardEditShellComponent } from './visitor-card/visitor-card-edit-shell/visitor-card-edit-shell.component';
import { DeviceListShellComponent } from './device/device-list-shell/device-list-shell.component';
import { DeviceEditShellComponent } from './device/device-edit-shell/device-edit-shell.component';
import { DepartmentListShellComponent } from './department/department-list-shell/department-list-shell.component';
import { DepartmentEditShellComponent } from './department/department-edit-shell/department-edit-shell.component';
import { DepartmentStaffListShellComponent } from './department-staff/department-staff-list-shell/department-staff-list-shell.component';
import { DepartmentStaffEditShellComponent } from './department-staff/department-staff-edit-shell/department-staff-edit-shell.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/settings/user',
    pathMatch: 'full'
  },
  {
    path: 'user',
    component: UserListShellComponent
  },
  {
    path: 'user/:id/edit',
    component: UserEditShellComponent
  },
  {
    path: 'role',
    component: RoleListShellComponent
  },
  {
    path: 'role/:id/edit',
    component: RoleEditShellComponent
  },
  {
    path: 'visitor-card',
    component: VisitorCardListShellComponent
  },
  {
    path: 'visitor-card/:id/edit',
    component: VisitorCardEditShellComponent
  },
  {
    path: 'device',
    component: DeviceListShellComponent
  },
  {
    path: 'device/:id/edit',
    component: DeviceEditShellComponent
  },
  {
    path: 'department',
    component: DepartmentListShellComponent
  },
  {
    path: 'department/:id/edit',
    component: DepartmentEditShellComponent
  },
  {
    path: 'department-staff',
    component: DepartmentStaffListShellComponent
  },
  {
    path: 'department-staff/:id/edit',
    component: DepartmentStaffEditShellComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingsRoutingModule { }
