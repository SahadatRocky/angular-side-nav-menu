import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SettingsRoutingModule } from './settings-routing.module';
import { UserListShellComponent } from './user/user-list-shell/user-list-shell.component';
import { UserListComponent } from './user/user-list/user-list.component';
import { SharedModule } from '../shared/shared.module';
import { UserEditComponent } from './user/user-edit/user-edit.component';
import { UserEditShellComponent } from './user/user-edit-shell/user-edit-shell.component';
import { RoleListShellComponent } from './role/role-list-shell/role-list-shell.component';
import { RoleListComponent } from './role/role-list/role-list.component';
import { RoleEditComponent } from './role/role-edit/role-edit.component';
import { RoleEditShellComponent } from './role/role-edit-shell/role-edit-shell.component';
import { VisitorCardListShellComponent } from './visitor-card/visitor-card-list-shell/visitor-card-list-shell.component';
import { VisitorCardListComponent } from './visitor-card/visitor-card-list/visitor-card-list.component';
import { VisitorCardEditShellComponent } from './visitor-card/visitor-card-edit-shell/visitor-card-edit-shell.component';
import { VisitorCardEditComponent } from './visitor-card/visitor-card-edit/visitor-card-edit.component';
import { DeviceListShellComponent } from './device/device-list-shell/device-list-shell.component';
import { DeviceListComponent } from './device/device-list/device-list.component';
import { DeviceEditShellComponent } from './device/device-edit-shell/device-edit-shell.component';
import { DeviceEditComponent } from './device/device-edit/device-edit.component';
import { DepartmentListShellComponent } from './department/department-list-shell/department-list-shell.component';
import { DepartmentListComponent } from './department/department-list/department-list.component';
import { DepartmentEditShellComponent } from './department/department-edit-shell/department-edit-shell.component';
import { DepartmentEditComponent } from './department/department-edit/department-edit.component';
import { DepartmentStaffListShellComponent } from './department-staff/department-staff-list-shell/department-staff-list-shell.component';
import { DepartmentStaffListComponent } from './department-staff/department-staff-list/department-staff-list.component';
import { DepartmentStaffEditShellComponent } from './department-staff/department-staff-edit-shell/department-staff-edit-shell.component';
import { DepartmentStaffEditComponent } from './department-staff/department-staff-edit/department-staff-edit.component';


@NgModule({
  declarations: [
  
    UserListShellComponent,
    UserListComponent,
    UserEditComponent,
    UserEditShellComponent,
    RoleListShellComponent,
    RoleListComponent,
    RoleEditComponent,
    RoleEditShellComponent,
    VisitorCardListShellComponent,
    VisitorCardListComponent,
    VisitorCardEditShellComponent,
    VisitorCardEditComponent,
    DeviceListShellComponent,
    DeviceListComponent,
    DeviceEditShellComponent,
    DeviceEditComponent,
    DepartmentListShellComponent,
    DepartmentListComponent,
    DepartmentEditShellComponent,
    DepartmentEditComponent,
    DepartmentStaffListShellComponent,
    DepartmentStaffListComponent,
    DepartmentStaffEditShellComponent,
    DepartmentStaffEditComponent
  ],
  imports: [
    CommonModule,
    SettingsRoutingModule,
    SharedModule
  ]
})
export class SettingsModule { }
