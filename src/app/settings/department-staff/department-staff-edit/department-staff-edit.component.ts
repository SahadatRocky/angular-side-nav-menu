import { Location } from '@angular/common';
import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable, debounceTime, map, startWith } from 'rxjs';
import { autocompleteValidator } from '../../../core/validators/autocomplete.validator';
import { DepartmentStaff } from '../models/department-staff.model';
import { OrganizationType } from '../../../core/models/organization-type.model';
import { DepartmentType } from '../../../core/models/department-type.model';
import { UserType } from '../../../core/models/user-type.model';


@Component({
  selector: 'app-department-staff-edit',
  templateUrl: './department-staff-edit.component.html',
  styleUrls: ['./department-staff-edit.component.scss']
})
export class DepartmentStaffEditComponent implements OnChanges {

  form!: FormGroup;

  typeGroup = new FormGroup({
    approverTypeId: new FormControl('')
  })

  @Input() departmentStaffId!: string;
  @Input() departmentStaff!: DepartmentStaff | null;
  @Input() organizationTypeList!: OrganizationType[];
  filteredOrganizationTypeList!: Observable<OrganizationType[]>;

  @Input() departmentTypeList!: DepartmentType[];
  filteredDepartmentTypeList!: Observable<DepartmentType[]>;
  
  @Input() userTypeList!: UserType[];
  filteredUserTypeList!: Observable<UserType[]>;

  @Output() onCreate = new EventEmitter<DepartmentStaff>();
  @Output() onUpdate = new EventEmitter<DepartmentStaff>();
  

  constructor(
    private fb: FormBuilder
    , private readonly location: Location
  ) { }

  ngOnChanges(changes: SimpleChanges): void {

    this.initForm();

    if (this.organizationTypeList?.length > 0) {
      this.organizationId.setValidators([Validators.required, autocompleteValidator(this.organizationTypeList)])
      this.setOrganizationList()
    }

    if (this.departmentTypeList?.length > 0) {
      this.departmentId.setValidators([Validators.required, autocompleteValidator(this.departmentTypeList)])
      this.setDepartmentList()
    }

    if (this.userTypeList?.length > 0) {
      this.userId.setValidators([Validators.required, autocompleteValidator(this.userTypeList)])
      this.setUserList()
    }

    if (this.departmentStaff) {
      this.form.patchValue({ ...this.departmentStaff });
    }
  }

  initForm() {

    if (this.form) return;
    this.form = this.fb.group({
      organizationId: ['',[Validators.required]],
      departmentId: ['', [Validators.required]],
      userId: ['', [Validators.required]],
      isActive: ['', []],
    });
  }

  setOrganizationList() {
    if (this.organizationId) {
      this.filteredOrganizationTypeList = this.organizationId.valueChanges.pipe(
        startWith(''),
        debounceTime(200),
        map(value =>
          this.organizationTypeList?.filter((option: OrganizationType) => option?.name?.toLowerCase().includes(value.toString().toLowerCase())) ?? ''
        ));
    }
  }

  setDepartmentList(){
    if (this.departmentId) {
      this.filteredDepartmentTypeList = this.departmentId.valueChanges.pipe(
        startWith(''),
        debounceTime(200),
        map(value =>
          this.departmentTypeList?.filter((option: DepartmentType) => option?.name?.toLowerCase().includes(value.toString().toLowerCase())) ?? ''
        ));
    }
  }

  setUserList(){
    if (this.userId) {
      this.filteredUserTypeList = this.userId.valueChanges.pipe(
        startWith(''),
        debounceTime(200),
        map(value =>
          this.userTypeList?.filter((option: OrganizationType) => option?.name?.toLowerCase().includes(value.toString().toLowerCase())) ?? ''
        ));
    }
  }


  displayOrganizationTypeAutocompleteValue(e: any): string {
    return e ? e.name : '';
  }

  displayDepartmentAutocompleteValue(e: any): string {
    return e ? e.name : '';
  }

  displayUserTypeAutocompleteValue(e: any): string {
    return e ? e.name : '';
  }

  back() {
    this.location.back();
  }

  submit() {
    const data: DepartmentStaff = {
      ...this.form.value
      , organizationId: this.organizationId?.value['id'] ?? ''
      , departmentNameId:this.departmentId?.value['id'] ?? ''
      , userId: this.userId?.value['id'] ?? ''
      , status: 'true'
      , version: 0
    }

    this.onCreate.emit(data)
  }

  edit() {
    const data: DepartmentStaff = {
      ...this.departmentStaff
      , ...this.form.value
      , organizationId: this.organizationId?.value['id'] ?? ''
      , departmentNameId:this.departmentId?.value['id'] ?? ''
      , userId: this.userId?.value['id'] ?? ''
    }

    this.onUpdate.emit(data)
  }

  get organizationId() {
    return this.form.controls['organizationId'];
  }

  get departmentId() {
    return this.form.controls['departmentId'];
  }

  get userId(){
    return this.form.controls['userId'];
  }

  get isActive() {
    return this.form.controls['isActive'];
  }

}

