import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DepartmentStaffEditComponent } from './department-staff-edit.component';

describe('DepartmentStaffEditComponent', () => {
  let component: DepartmentStaffEditComponent;
  let fixture: ComponentFixture<DepartmentStaffEditComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DepartmentStaffEditComponent]
    });
    fixture = TestBed.createComponent(DepartmentStaffEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
