import { Component, Input, OnChanges, OnInit, SimpleChanges, signal } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { LoadingService } from '../../../core/services/loading.service';
import { NewDepartmentType as OrganizationType } from '../../../core/models/new-department-type.model';
import { DepartmentStaff } from '../models/department-staff.model';
import { DepartmentStaffService } from '../services/department-staff.service';
import { DepartmentType } from '../../../core/models/department-type.model';
import { UserType } from '../../../core/models/user-type.model';

@Component({
  selector: 'app-department-staff-edit-shell',
  templateUrl: './department-staff-edit-shell.component.html',
  styleUrls: ['./department-staff-edit-shell.component.scss']
})
export class DepartmentStaffEditShellComponent implements OnInit, OnChanges {

  @Input() id!: string;

  departmentStaff = signal<DepartmentStaff | null>(null)

  organizationTypeList = signal<OrganizationType[]>([
    {
     id : '1', 
     name: 'Business Automation Ltd'
    }
  ]);

  departmentTypeList = signal<DepartmentType[]>([
    {
     id : '2', 
     name: 'Indutry 4.0'
    }
  ]);

  userTypeList = signal<UserType[]>([
    {
     id : '3', 
     name: 'Sahadat'
    }
  ]);
  
  constructor(
    private readonly departmentStaffService: DepartmentStaffService
    , private readonly loadingService: LoadingService
    , private readonly router: Router
    , private readonly dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.getDepartmentStaff()
    this.getAllOrganization()
    this.getAllDepartment()
    this.getAllUser()
  }

  ngOnChanges(changes: SimpleChanges): void { }

  getDepartmentStaff() {
    if (this.id && this.id != '0') {
      this.departmentStaffService.getDepartmentStaff(this.id).subscribe({
        next: departmentStaff => this.departmentStaff.set(departmentStaff),
        error: error => { }
      })
    }
  }

  getAllOrganization(){
    // this.reviewTypeService.getAllReviewType().subscribe({
    //   next: branchList => this.reviewTypeList.set(branchList),
    //   error: error => this.reviewTypeList.set([])
    // })
  }

  getAllDepartment(){
    // this.reviewTypeService.getAllReviewType().subscribe({
    //   next: branchList => this.reviewTypeList.set(branchList),
    //   error: error => this.reviewTypeList.set([])
    // })
  }

  getAllUser(){
    // this.reviewTypeService.getAllReviewType().subscribe({
    //   next: branchList => this.reviewTypeList.set(branchList),
    //   error: error => this.reviewTypeList.set([])
    // })
  }

  
  createDepartment(departmentStaff: DepartmentStaff) {
    this.loadingService.showLoaderUntilCompleted(
      this.departmentStaffService.createDepartmentStaff(departmentStaff)
    ).subscribe({
      next: departmentStaff => {
        // this.messageService.showSuccessMessage('User Created Successfully');
        this.router.navigateByUrl(`/department-staff`, { replaceUrl: true })
      },
      error: error => {
        console.log(error)
        // this.messageService.showErrorMessage('Failed To Create User')
      }
    })
  }

  updateDepartment(departmentStaff: DepartmentStaff) {
    this.loadingService.showLoaderUntilCompleted(
      this.departmentStaffService.updateDepartmentStaff(this.id, departmentStaff)
    ).subscribe({
      next: departmentStaff => {
        // this.messageService.showSuccessMessage('User Updated Successfully');
        this.departmentStaff.set(departmentStaff)
        // this.router.navigateByUrl(`/user/${user.id}/edit`)
      },
      error: error => {
        console.log(error)
        // this.messageService.showErrorMessage('Failed To Update User')
      }
    })
  }
}
