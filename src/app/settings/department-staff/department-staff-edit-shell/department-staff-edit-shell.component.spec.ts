import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DepartmentStaffEditShellComponent } from './department-staff-edit-shell.component';

describe('DepartmentStaffEditShellComponent', () => {
  let component: DepartmentStaffEditShellComponent;
  let fixture: ComponentFixture<DepartmentStaffEditShellComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DepartmentStaffEditShellComponent]
    });
    fixture = TestBed.createComponent(DepartmentStaffEditShellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
