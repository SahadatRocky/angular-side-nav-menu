import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DepartmentStaffListComponent } from './department-staff-list.component';

describe('DepartmentStaffListComponent', () => {
  let component: DepartmentStaffListComponent;
  let fixture: ComponentFixture<DepartmentStaffListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DepartmentStaffListComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(DepartmentStaffListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
