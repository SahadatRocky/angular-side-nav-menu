import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DepartmentStaffListShellComponent } from './department-staff-list-shell.component';

describe('DepartmentListShellComponent', () => {
  let component: DepartmentStaffListShellComponent;
  let fixture: ComponentFixture<DepartmentStaffListShellComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DepartmentStaffListShellComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(DepartmentStaffListShellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
