import { Component, OnInit, signal } from '@angular/core';
import { LoadingService } from '../../../core/services/loading.service';
import { PageChangeInfo } from '../../../core/models/pageChangeInfo';
import { Router } from '@angular/router';
import { DepartmentStaff } from '../models/department-staff.model';
import { DepartmentStaffService } from '../services/department-staff.service';

@Component({
  selector: 'app-department-staff-list-shell',
  templateUrl: './department-staff-list-shell.component.html',
  styleUrl: './department-staff-list-shell.component.css'
})
export class DepartmentStaffListShellComponent implements OnInit {
     
  departmentStaffList = signal<DepartmentStaff[]>([]);
  pageSize = signal<number>(0);
  totalElements = signal<number>(0);
  constructor(
    private readonly departmentStaffService: DepartmentStaffService
    , private readonly loadingService: LoadingService
    , private router : Router){}

  ngOnInit(): void {
    this.getDepartmentStaffList();
  }

  getDepartmentStaffList(pageIndex = 0, pageSize = 10){
      this.departmentStaffService.getDepartmentStaffList(pageIndex, pageSize).subscribe({
        next: departmentStaffs => {
          this.departmentStaffList.set(departmentStaffs.content);
          this.totalElements.set(departmentStaffs.totalElements);
          this.pageSize.set(departmentStaffs.size);
        },
        error: error => {
          this.departmentStaffList.set([]);
          this.totalElements.set(0);
          this.pageSize.set(0);
        }
      }
      );
  }

  onPageChanged(pageChangeInfo: PageChangeInfo): void {
    this.getDepartmentStaffList(pageChangeInfo.pageIndex, pageChangeInfo.pageSize)
  }

  clickedButton(id: string) {
    // console.log(id);
    this.router.navigateByUrl(`/settings/department-staff/${id}/edit`)
  }

}
