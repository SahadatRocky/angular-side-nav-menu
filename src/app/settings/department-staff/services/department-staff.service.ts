import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { ErrorHandlerService } from "../../../core/services/error-handler.service";
import { baseUrl } from "../../../../environments/environment";
import { Observable, catchError } from "rxjs";
import { DepartmentStaffs } from "../models/department-staffs.model";
import { DepartmentStaff } from "../models/department-staff.model";


@Injectable({
    providedIn: 'root'
})
export class DepartmentStaffService{
     
   resourceUrl !: string;
   constructor(private http: HttpClient,
    private readonly errorHandlerService: ErrorHandlerService){
        this.resourceUrl = `${baseUrl}/department-staff`;
   }

   getDepartmentStaffList(pageIndex: number, pageSize: number): Observable<DepartmentStaffs> {
    return this.http.get<DepartmentStaffs>(`${this.resourceUrl}`, {
      params: {
        page: pageIndex,
        size: pageSize
      }
    }).pipe(
      catchError(this.errorHandlerService.handleError)
    )
  }

  getDepartmentStaff(departmentId: string): Observable<DepartmentStaff> {
    return this.http.get<DepartmentStaff>(`${this.resourceUrl}/${departmentId}`).pipe(
      // tap(response => console.log(response)),
      catchError(this.errorHandlerService.handleError)
    )
  }


  createDepartmentStaff(departmentStaff: DepartmentStaff): Observable<DepartmentStaff> {
    return this.http.post<DepartmentStaff>(this.resourceUrl, departmentStaff).pipe(
      // tap(response => console.log(response)),
      catchError(this.errorHandlerService.handleError)
    )
  }

  updateDepartmentStaff(departmentStaffId: string, departmentStaff: DepartmentStaff): Observable<DepartmentStaff> {
    return this.http.put<DepartmentStaff>(`${this.resourceUrl}/${departmentStaffId}`, departmentStaff).pipe(
      // tap(response => console.log(response)),
      catchError(this.errorHandlerService.handleError)
    )
  }

  getParams(myMap : Map<string, any>): HttpParams{
    let params = new HttpParams();
    myMap.forEach((value : any, key: any)=> {
        params = params.append(key, value);
    }) 
    return params;
  }

  departmentStaffFilterRequest(pageIndex: number, pageSize: number, myMap : Map<string, any>): Observable<DepartmentStaffs>{
    
    myMap.set('page', pageIndex);
    myMap.set('size', pageSize);
    let params = this.getParams(myMap);
    return this.http.get<DepartmentStaffs>(`${this.resourceUrl}/search`, {params})
        .pipe(
            catchError(this.errorHandlerService.handleError)
        );
  }

}