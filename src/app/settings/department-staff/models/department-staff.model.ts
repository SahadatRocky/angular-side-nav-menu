

export interface DepartmentStaff{
    organizationName : string;
    departmentName : string;
    user : string;
    status : boolean; 
}