import { DepartmentStaff } from "./department-staff.model";

export interface DepartmentStaffs{
    content: DepartmentStaff[];
    totalElements: number;
    size: number;
}