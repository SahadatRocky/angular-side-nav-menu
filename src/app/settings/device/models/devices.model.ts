import { Device } from "./device.model";

export interface Devices{
    content: Device[];
    totalElements: number;
    size: number;
}