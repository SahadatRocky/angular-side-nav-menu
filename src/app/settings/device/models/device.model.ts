

export interface Device{
    deviceLocation : string;
    description : string;
    deviceType : string;
    status : boolean; 
}