import { Component, OnInit, signal } from '@angular/core';
import { LoadingService } from '../../../core/services/loading.service';
import { PageChangeInfo } from '../../../core/models/pageChangeInfo';
import { Router } from '@angular/router';
import { Device } from '../models/device.model';
import { DeviceService } from '../services/device.service';

@Component({
  selector: 'app-device-list-shell',
  templateUrl: './device-list-shell.component.html',
  styleUrl: './device-list-shell.component.css'
})
export class DeviceListShellComponent implements OnInit {
     
  deviceList = signal<Device[]>([]);
  pageSize = signal<number>(0);
  totalElements = signal<number>(0);
  constructor(
    private readonly deviceService: DeviceService
    , private readonly loadingService: LoadingService
    , private router : Router){}

  ngOnInit(): void {
    this.getDeviceList();
  }

  getDeviceList(pageIndex = 0, pageSize = 10){
      this.deviceService.getDeviceList(pageIndex, pageSize).subscribe({
        next: devices => {
          this.deviceList.set(devices.content);
          this.totalElements.set(devices.totalElements);
          this.pageSize.set(devices.size);
        },
        error: error => {
          this.deviceList.set([]);
          this.totalElements.set(0);
          this.pageSize.set(0);
        }
      }
      );
  }

  onPageChanged(pageChangeInfo: PageChangeInfo): void {
    this.getDeviceList(pageChangeInfo.pageIndex, pageChangeInfo.pageSize)
  }

  clickedButton(id: string) {
    // console.log(id);
    this.router.navigateByUrl(`/settings/device/${id}/edit`)
  }

}
