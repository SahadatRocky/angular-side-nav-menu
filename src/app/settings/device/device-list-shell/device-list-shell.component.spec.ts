import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeviceListShellComponent } from './device-list-shell.component';

describe('DeviceListShellComponent', () => {
  let component: DeviceListShellComponent;
  let fixture: ComponentFixture<DeviceListShellComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DeviceListShellComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(DeviceListShellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
