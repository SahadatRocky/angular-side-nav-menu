import { Component, Input, OnChanges, OnInit, SimpleChanges, signal } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { LoadingService } from '../../../core/services/loading.service';
import { DeviceService } from '../services/device.service';
import { Device } from '../models/device.model';
import { DeviceType } from '../../../core/models/device-type.model';

@Component({
  selector: 'app-device-edit-shell',
  templateUrl: './device-edit-shell.component.html',
  styleUrls: ['./device-edit-shell.component.scss']
})
export class DeviceEditShellComponent implements OnInit, OnChanges {

  @Input() id!: string;

  device = signal<Device | null>(null)
  deviceTypeList = signal<DeviceType[]>([
    {
     id : '1', 
     deviceTypeName: 'A DeviceType'
    },
    {
      id : '2', 
      deviceTypeName: 'B DeviceType'
     }
  ]);
  
  constructor(
    private readonly deviceService: DeviceService
    , private readonly loadingService: LoadingService
    , private readonly router: Router
    , private readonly dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.getDevice()
    this.getAllDeviceType()
  }

  ngOnChanges(changes: SimpleChanges): void { }

  getDevice() {
    if (this.id && this.id != '0') {
      this.deviceService.getDevice(this.id).subscribe({
        next: device => this.device.set(device),
        error: error => { }
      })
    }
  }

  getAllDeviceType(){
    // this.reviewTypeService.getAllReviewType().subscribe({
    //   next: branchList => this.reviewTypeList.set(branchList),
    //   error: error => this.reviewTypeList.set([])
    // })
  }

  
  createDevice(device: Device) {
    this.loadingService.showLoaderUntilCompleted(
      this.deviceService.createDevice(device)
    ).subscribe({
      next: device => {
        // this.messageService.showSuccessMessage('User Created Successfully');
        this.router.navigateByUrl(`/device`, { replaceUrl: true })
      },
      error: error => {
        console.log(error)
        // this.messageService.showErrorMessage('Failed To Create User')
      }
    })
  }

  updateDevice(device: Device) {
    this.loadingService.showLoaderUntilCompleted(
      this.deviceService.updateDevice(this.id, device)
    ).subscribe({
      next: device => {
        // this.messageService.showSuccessMessage('User Updated Successfully');
        this.device.set(device)
        // this.router.navigateByUrl(`/user/${user.id}/edit`)
      },
      error: error => {
        console.log(error)
        // this.messageService.showErrorMessage('Failed To Update User')
      }
    })
  }
}
