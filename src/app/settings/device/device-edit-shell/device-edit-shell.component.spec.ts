import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeviceEditShellComponent } from './device-edit-shell.component';

describe('DeviceEditShellComponent', () => {
  let component: DeviceEditShellComponent;
  let fixture: ComponentFixture<DeviceEditShellComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DeviceEditShellComponent]
    });
    fixture = TestBed.createComponent(DeviceEditShellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
