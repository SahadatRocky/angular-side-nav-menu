import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { ErrorHandlerService } from "../../../core/services/error-handler.service";
import { baseUrl } from "../../../../environments/environment";
import { Observable, catchError } from "rxjs";
import { Devices } from "../models/devices.model";
import { Device } from "../models/device.model";


@Injectable({
    providedIn: 'root'
})
export class DeviceService{
     
   resourceUrl !: string;
   constructor(private http: HttpClient,
    private readonly errorHandlerService: ErrorHandlerService){
        this.resourceUrl = `${baseUrl}/device`;
   }

   getDeviceList(pageIndex: number, pageSize: number): Observable<Devices> {
    return this.http.get<Devices>(`${this.resourceUrl}`, {
      params: {
        page: pageIndex,
        size: pageSize
      }
    }).pipe(
      catchError(this.errorHandlerService.handleError)
    )
  }

  getDevice(DeviceId: string): Observable<Device> {
    return this.http.get<Device>(`${this.resourceUrl}/${DeviceId}`).pipe(
      // tap(response => console.log(response)),
      catchError(this.errorHandlerService.handleError)
    )
  }


  createDevice(device: Device): Observable<Device> {
    return this.http.post<Device>(this.resourceUrl, device).pipe(
      // tap(response => console.log(response)),
      catchError(this.errorHandlerService.handleError)
    )
  }

  updateDevice(deviceId: string, device: Device): Observable<Device> {
    return this.http.put<Device>(`${this.resourceUrl}/${deviceId}`, device).pipe(
      // tap(response => console.log(response)),
      catchError(this.errorHandlerService.handleError)
    )
  }

  getParams(myMap : Map<string, any>): HttpParams{
    let params = new HttpParams();
    myMap.forEach((value : any, key: any)=> {
        params = params.append(key, value);
    }) 
    return params;
  }

  deviceFilterRequest(pageIndex: number, pageSize: number, myMap : Map<string, any>): Observable<Devices>{
    
    myMap.set('page', pageIndex);
    myMap.set('size', pageSize);
    let params = this.getParams(myMap);
    return this.http.get<Devices>(`${this.resourceUrl}/search`, {params})
        .pipe(
            catchError(this.errorHandlerService.handleError)
        );
  }

}