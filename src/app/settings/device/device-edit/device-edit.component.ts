import { Location } from '@angular/common';
import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ReviewType } from '../../../core/models/review-type.model';
import { Observable, debounceTime, map, startWith } from 'rxjs';
import { autocompleteValidator } from '../../../core/validators/autocomplete.validator';
import { Device } from '../models/device.model';
import { DeviceType } from '../../../core/models/device-type.model';


@Component({
  selector: 'app-device-edit',
  templateUrl: './device-edit.component.html',
  styleUrls: ['./device-edit.component.scss']
})
export class DeviceEditComponent implements OnChanges {

  form!: FormGroup;

  typeGroup = new FormGroup({
    approverTypeId: new FormControl('')
  })

  @Input() deviceId!: string;
  @Input() device!: Device | null;
  @Input() deviceTypeList!: DeviceType[];

  @Output() onCreate = new EventEmitter<Device>();
  @Output() onUpdate = new EventEmitter<Device>();
  filtereddDeviceTypeList!: Observable<DeviceType[]>;

  constructor(
    private fb: FormBuilder
    , private readonly location: Location
  ) { }

  ngOnChanges(changes: SimpleChanges): void {

    this.initForm();

    if (this.deviceTypeList?.length > 0) {
      this.deviceType.setValidators([Validators.required, autocompleteValidator(this.deviceTypeList)])
      this.setDeviceTypeList()
    }

    if (this.device) {
      this.form.patchValue({ ...this.device });
    }
  }

  initForm() {

    if (this.form) return;
    this.form = this.fb.group({
      deviceLocation: ['', [Validators.required]],
      deviceDescription: ['', [Validators.required]],
      deviceType:[''],
      isActive: ['', []],
    });
  }

  setDeviceTypeList() {
    if (this.deviceType) {
      this.filtereddDeviceTypeList = this.deviceType.valueChanges.pipe(
        startWith(''),
        debounceTime(200),
        map(value =>
          this.deviceTypeList?.filter((option: DeviceType) => option?.deviceTypeName?.toLowerCase().includes(value.toString().toLowerCase())) ?? ''
        ));
    }
  }


  public isValidNumber(event: any): boolean {
    const charCode = event.keyCode;
    if (charCode == 8 || charCode == 46 || (charCode >= 48 && charCode <= 57) || (charCode >= 96 && charCode <= 105)) {
      return true;
    }
    return false;
  }

  displayDeviceTypeAutocompleteValue(e: any): string {
    return e ? e.deviceTypeName : '';
  }

  back() {
    this.location.back();
  }

  submit() {
    const data: Device = {
      ...this.form.value
      , deviceTypeId: this.deviceType?.value['id'] ?? ''
      , status: 'true'
      , version: 0
    }

    this.onCreate.emit(data)
  }

  edit() {
    const data: Device = {
      ...this.device
      , ...this.form.value
      , deviceTypeId: this.deviceType?.value['id'] ?? ''
    }

    this.onUpdate.emit(data)
  }


  get deviceLocation() {
    return this.form.controls['deviceLocation'];
  }
  get deviceDescription() {
    return this.form.controls['deviceDescription'];
  }

  get deviceType() {
    return this.form.controls['deviceType'];
  }

  get isActive() {
    return this.form.controls['isActive'];
  }


}

