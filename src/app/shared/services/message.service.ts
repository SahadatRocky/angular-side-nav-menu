import { Injectable } from "@angular/core";
import { MatSnackBar, MatSnackBarConfig } from "@angular/material/snack-bar";
import { SnackbarMessageInfo } from "../models/snackbar-message-info.model";
import { snackbar_message_type } from "../../core/constant/messages";

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  config = {
    verticalPosition: 'top',
    horizontalPosition: 'center'
  } as MatSnackBarConfig;

  constructor(private readonly snackBar: MatSnackBar) { }

  showMessage(info: SnackbarMessageInfo): void {

    // this.messageSubject.next(messageInfo);

    let config = {
      verticalPosition: 'top',
      horizontalPosition: 'center'
    } as MatSnackBarConfig;

    if (info.type === snackbar_message_type.SUCCESS_TYPE) {

      config = {
        ...config
        , panelClass: ['mat-green-snackbar']
      } as MatSnackBarConfig;

    } else if (info.type === snackbar_message_type.WARN_TYPE) {

      config = {
        ...config
        , panelClass: ['mat-orange-snackbar']
      } as MatSnackBarConfig;

    } else if (info.type === snackbar_message_type.ERROR_TYPE) {

      config = {
        ...config
        , panelClass: ['mat-red-snackbar']
      } as MatSnackBarConfig;

    }

    this.snackBar.open(info.message, '', config);

  }

  showSuccessMessage(message: string): void {

    this.config = {
      ...this.config
      , panelClass: ['mat-green-snackbar']
    } as MatSnackBarConfig;
    this.openSnackBar(message);
  }

  showErrorMessage(message: string) {

    this.config = {
      ...this.config
      , panelClass: ['mat-red-snackbar']
    } as MatSnackBarConfig;
    this.openSnackBar(message);
  }

  showWarningMessage(message: string) {

    this.config = {
      ...this.config
      , panelClass: ['mat-orange-snackbar']
    } as MatSnackBarConfig;
    this.openSnackBar(message);
  }

  openSnackBar(message: string): void {
    this.snackBar.open(message, '', this.config);
  }

}
