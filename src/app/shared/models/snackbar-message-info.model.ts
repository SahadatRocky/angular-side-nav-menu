
export interface SnackbarMessageInfo {

  type: string;
  message: string;
}
