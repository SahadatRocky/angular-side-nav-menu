
export interface NavItem {
  displayName: string;
  allowedGroup?: string[];
  route?: string;
  childrens?: NavItem[];
  imagePath?: string;
  type?: string;
  queryParamKey?: string;
  queryParamValue?: string;
}

export const navItems: NavItem[] = [
  {
    displayName: 'Dashboard',
    route: '/dashboard',
    imagePath: '../../../assets/images/dashboard_white.svg',
    // allowedGroup: ['CORPORATE_USER'],
  },
  // {
  //   displayName: 'Report',
  //   route: '/insight',
  //   imagePath: '../../../assets/images/report.svg',
  //   allowedGroup: ['CORPORATE_USER'],
  // },

  {
    displayName: 'Settings',
    // route: '/settings',
    // imagePath: '../../../assets/images/setting_white.svg',
    // allowedGroup: ['ROOT'],
    childrens: [
      {
        displayName: 'Roles',
        route: '/settings/role',
        // allowedGroup: ['ROOT'],
      },
      {
        displayName: 'Users',
        route: '/settings/user',
        // allowedGroup: ['ROOT'],
      },
      {
        displayName: 'Visitor Card',
        route: '/settings/visitor-card',
        // allowedGroup: ['ROOT'],
      },
      {
        displayName: 'Device',
        route: '/settings/device',
        // allowedGroup: ['ROOT'],
      },
      {
        displayName: 'Department',
        route: '/settings/department',
        // allowedGroup: ['ROOT'],
      },
      {
        displayName: 'Department Staff',
        route: '/settings/department-staff',
        // allowedGroup: ['ROOT'],
      },
      {
        displayName: 'Organization',
        route: '/settings/organization',
        // allowedGroup: ['ROOT'],
      },
      {
        displayName: 'Gate Info',
        route: '/settings/gate-info',
        // allowedGroup: ['ROOT'],
      },
    ],
  },
];
