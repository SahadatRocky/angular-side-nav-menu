
export interface DepartmentType{
   id: string;
   name: string;
}