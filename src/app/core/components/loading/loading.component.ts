
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { NavigationCancel, NavigationEnd, NavigationError, NavigationStart, RouteConfigLoadEnd, RouteConfigLoadStart, Router } from '@angular/router';
import { LoadingService } from '../../services/loading.service';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoadingComponent implements OnInit {
  
  constructor(public loadingService: LoadingService
    , private router: Router) {
  }

  ngOnInit(): void {
    
    this.router.events.subscribe(event => {
        if(event  instanceof NavigationStart || event instanceof RouteConfigLoadStart){
          
          this.loadingService.loadingOn();

        }
        else if(event instanceof NavigationEnd || event instanceof NavigationCancel || event instanceof NavigationError){
          
          this.loadingService.loadingOff();

        }else if(event instanceof RouteConfigLoadEnd){
            // console.log('An event triggered when a route has been lazy loaded. We do not want to stop loading here')
        }
    });

  }
}
