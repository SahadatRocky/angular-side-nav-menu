import { ChangeDetectionStrategy, Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-confirmation-dialog',
  templateUrl: './confirmation-dialog.component.html',
  styleUrls: ['./confirmation-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ConfirmationDialogComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
  private dialogRef: MatDialogRef<ConfirmationDialogComponent>,) { }

  ngOnInit(): void {
  }

  onYes() {
    this.dialogRef.close('YES');
  }

  onNo() {
    this.dialogRef.close();
  }

}

export function openConfirmationDialog(dialog: MatDialog, data?: any): Observable<any> {

  const config = new MatDialogConfig()

  config.disableClose = true;
  config.autoFocus = true;
  config.panelClass = 'confirmation-modal-panel';
  config.backdropClass = 'backdrop-modal-panel';

  config.data = data ?? {};

  const dialogRef = dialog.open(ConfirmationDialogComponent, config)

  return dialogRef.afterClosed();

}



/*


private readonly dialog: MatDialog

openConfirmationDialog(this.dialog, {
      question: 'Do you want to copy this process?',
    })
      .subscribe(data => {

        if (data) {
          console.log(data)
        }

      });

*/
