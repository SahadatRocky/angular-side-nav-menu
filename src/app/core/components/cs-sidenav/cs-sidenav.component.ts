import { animate, state, style, transition, trigger } from '@angular/animations';
import { AfterViewInit, Component, HostListener, OnDestroy, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Title } from '@angular/platform-browser';
// import { KeycloakService } from 'keycloak-angular';
import { from, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { VMS_VERSION, NAV_CHILD_HEADER } from '../../constant/constants';
import { navItems } from '../../nav';


@Component({
  selector: 'cs-sidenav',
  templateUrl: './cs-sidenav.component.html',
  styleUrls: ['./cs-sidenav.component.scss'],
  //animations for nav items that has childrens
  animations: [
    trigger('animationShowHide', [
      state('close', style({ height: '0px', overflow: 'hidden' })),
      state('open', style({ height: '*', overflow: 'hidden' })),
      transition('open <=> close', animate('900ms ease-in-out')),
    ]),
    trigger('animationRotate', [
      state('close', style({ transform: 'rotate(0)' })),
      state('open', style({ transform: 'rotate(-180deg)' })),
      transition('open <=> close', animate('900ms ease-in-out')),
    ]),
  ],
})
export class CsSidenavComponent implements OnInit, AfterViewInit, OnDestroy {
  vmsVersion = VMS_VERSION;
  headerType = NAV_CHILD_HEADER;

  // sideNavMode: MatDrawerMode = 'side';

  //for toggling nav item
  // @ViewChild('snav', { static: true }) snav!: MatSidenav;

  navs = navItems;
  //getting groupName from localStorage to show or hide nav item based on the group of user that logged in
  groupName: string;

  //this list is maintained to expand / collapse a nav item that has childrens
  expandedStatusList: Array<string> = [];
  //this list is maintained to update view when user selects a nav item that has childrens
  clickedStatusList: Array<boolean> = [];

  /*
  * routed events fires multiple time for a single transition
  * we are maintaining this to make sure we do our work once for a single transition
  */
  currentRoute = '';

  // initialPageLoadForClosingNav = true;
  // initialPageLoadForScreenSizeObserver = true;

  homePage = true;
  showRightSideNav = false;
  // sub!: Subscription;
  // dashboards: Dashboard[] = [];
  userFullName$!: Observable<string | null>;

  // isLargeScreen = true;
  isSideNavOpen = false;
  sideNavMode: string = 'side';

  dark = {
    backgroundColor: 'bg-indigo-800',   //bg-[#292e34]
    textColor: 'text-white'
  };

  //bg-black text-white
  //bg-[#292e34] text-white
  //[class.bg-gray-100]="blackTheme

  white = {
    backgroundColor: 'bg-white',
    textColor: 'text-slate-700'
  };

  blackTheme = true;

  constructor(private readonly router: Router
    , private titleService: Title
    // , protected readonly keycloakService: KeycloakService
    , private responsive: BreakpointObserver

  ) {

    //set user group name
    this.groupName = localStorage.getItem('groupName') ?? '';

    this.navs.forEach(nav => {
      console.log('nav:',nav);
      this.expandedStatusList.push('close');
      this.clickedStatusList.push(false);
    });

  }

  ngOnInit(): void {

    this.router.routeReuseStrategy.shouldReuseRoute = () => { return false; };

    //set page title
    this.titleService.setTitle(`VMS`);

    //observe screen size change
    this.observeScreenSizeChange();

    // this.setListenerForDashboards();
    this.setListenerForUserFullNameInfo();

    this.router.events.subscribe(event => {

      if (event instanceof NavigationEnd) {
        this.processNavigationEnd((event as NavigationEnd));
      }

    });

  }

  ngAfterViewInit() {
    // this.message = this.messagingService.notifyMessage;
    // this.messagingService.requestPermission()
  }

  ngOnDestroy(): void {

    // if (this.sub) this.sub.unsubscribe();

  }

  observeScreenSizeChange() {
    this.responsive.observe([
      Breakpoints.TabletPortrait,
      Breakpoints.TabletLandscape,
      Breakpoints.HandsetPortrait,
      Breakpoints.HandsetLandscape,
      Breakpoints.Medium,
      Breakpoints.Large,
      Breakpoints.XLarge
    ])
      .subscribe(result => {
        // console.log(result)

        this.sideNavMode = 'side';

        const breakpoints = result.breakpoints

        if (breakpoints[Breakpoints.TabletPortrait] || breakpoints[Breakpoints.HandsetPortrait] || breakpoints[Breakpoints.HandsetLandscape]) {

          this.sideNavMode = 'over';

        }
        else if (breakpoints[Breakpoints.TabletLandscape]
          || breakpoints[Breakpoints.Medium]
          || breakpoints[Breakpoints.Large]
          || breakpoints[Breakpoints.XLarge]) {

          this.sideNavMode = 'side';
        }

        // console.log(this.sideNavMode)
      })
  }

  toggleDrawer() {
    this.isSideNavOpen = !this.isSideNavOpen;
  }

  setListenerForUserFullNameInfo(): void {
    // this.user$ = this.appStore.select(selectUser);
    // this.userFullName$ = from(this.keycloakService.loadUserProfile())
    //   .pipe(
    //     // tap(userInfo => console.log(userInfo)),
    //     map(userInfo => {
    //       let fullName = '';
    //       if (userInfo.firstName) {
    //         fullName += userInfo.firstName ?? '';
    //       }
    //       if (userInfo.lastName) {
    //         fullName += ' ' + userInfo.lastName ?? '';
    //       }
    //       return fullName;
    //     })
    //   );
  }

  // setListenerForDashboards(): void {

  //   this.sub = this.appStore.select(selectDashboards).pipe(
  //     tap(dashboards => this.dashboards = dashboards)
  //   ).subscribe();

  // }

  /**
   * Listening for route navigation
   * For nav item that has children `routerLinkActive` won't work
   * so we are manually setting background
   * Toggling navbar after route transition completes
   * @param event
   */
  processNavigationEnd(event: NavigationEnd) {

    if (event.url) {

      this.groupName = localStorage.getItem('groupName') ?? '';

      if (event.url == '/') {
        this.homePage = true;
        // if(this.snav && this.snav.opened) {
        //   this.snav.close();
        // }
        return;
      }
      this.homePage = false;
      if (this.sideNavMode == 'over') {
        this.isSideNavOpen = false;
      }

      if (event.url == '/insight') {
        return;
      }

      if (!this.currentRoute) {
        this.handleNewRoute(event.url);
      }

      if (this.currentRoute && this.currentRoute !== event.url) {
        this.handleNewRoute(event.url);
      }
    }
  }

  handleNewRoute(url: string): void {
    this.processNewRouteNavigation(url);

    // if(this.isLargeScreen && this.snav) this.snav.open();

    this.setToDefault();

    if (url.startsWith('/dashboard')) {
      
    } 

    if (url.startsWith('/settings')) {
      // this.updateClicked(1);
    } 
    

  }

  /**
   * Toggle navbar after route transition completes
   * @param url
   */
  processNewRouteNavigation(url: string) {
    this.currentRoute = url;
  }

  /**
   * Setting all navbar option to default background color
   */
  setToDefault() {

    this.clickedStatusList = [];
    this.navs.forEach(nav => {
      this.clickedStatusList.push(false);
    });

  }

  /**
   * Updating clickedStatusList for the nav item that user clicked
   * @param index
   */
  updateClicked(index: number) {

    this.clickedStatusList[index] = true;

  }

  /**
   * Updating expandedStatusList for the nav item that user clicked
   * @param index
   */
  setExpanded(index: number) {

    this.expandedStatusList[index] = this.expandedStatusList[index] === 'close' ? 'open' : 'close';

  }

  logout() {

    // this.keycloakService.logout(window.location.origin);

    localStorage.clear();

  }

  createQueryParamJson(queryParamKey: string, queryParamValue: string | undefined) {
    let obj: any = {};
    obj[queryParamKey] = queryParamValue;
    return obj;
  }

  // scroll

  showScrollButton = false;

  @HostListener('window:scroll', [])
  onWindowScroll() {
    if (window.pageYOffset > 500) {
      this.showScrollButton = true;
    } else {
      this.showScrollButton = false;
    }
  }

  scrollToTop() {
    window.scrollTo(0, 0);
  }

}
