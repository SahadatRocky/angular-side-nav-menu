import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CsSidenavComponent } from './cs-sidenav.component';

describe('CsSidenavComponent', () => {
  let component: CsSidenavComponent;
  let fixture: ComponentFixture<CsSidenavComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CsSidenavComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CsSidenavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
