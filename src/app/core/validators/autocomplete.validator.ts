import { AbstractControl, ValidationErrors, ValidatorFn } from "@angular/forms";

//Branch[] | Division[]
export function autocompleteValidator(options: any): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    if(!control.value) return null;
    if (!options.includes(control.value)) {
      return { invalidValue: true };
    }
    return null;
  };
}
