
export const snackbar_message_type = {
  SUCCESS_TYPE: 'success',
  WARN_TYPE: 'warn',
  ERROR_TYPE: 'error',
};

export const success_message = {
  CREATED_SUCCESSFULLY: 'Created Successfully',
  CREATED_FAILURE: 'Failed To Create',
  FETCH_FAILURE: 'Failed To Fetch',
  // PROCESS_CREATE_SUCCESS: 'Process Created Successfully',
  // PROCESS_UPDATE_SUCCESS: 'Process Updated Successfully',
  // FLOW_CREATE_SUCCESS: 'Work Flow Created Successfully',
  // FLOW_UPDATE_SUCCESS: 'Work Flow Updated Successfully',
  // FLOW_COPY_SUCCESS: 'Work Flow Copied Successfully',
  // FLOW_PUBLISH_SUCCESS: 'Flow Published Successfully',
  // APPROVER_GROUP_USERS_CREATE_SUCCESS: 'Successfully added User to Approver Group',
  // APPROVER_GROUP_USERS_REMOVE_SUCCESS: 'Successfully removed User from Approver Group'
}

export const error_message = {
  
  PROCESS_CREATE_FAILURE: 'Failed to create process',
  PROCESS_UPDATE_FAILURE: 'Failed to update process',
  FLOW_CREATE_FAILURE: 'Failed to create work flow',
  FLOW_UPDATE_FAILURE: 'Failed to update work flow',
  FLOW_COPY_FAILURE: 'Failed to copy work flow',
  FLOW_PUBLISH_FAILURE: 'Failed to publish flow',
  APPROVER_GROUP_USERS_CREATE_FAILURE: 'Failed to add User to Approver Group',
  APPROVER_GROUP_USERS_REMOVE_FAILURE: 'Failed to remove User from Approver Group'
}
