import { Injectable, signal } from '@angular/core';
import { Observable, asapScheduler, concatMap, finalize, of, tap } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {

  loading = signal<boolean>(false);

  constructor() { }
  
  showLoaderUntilCompleted<T>(obs$: Observable<T>) : Observable<T> {
      return of(null)
      .pipe(
        tap(()=> this.loadingOn()),
        concatMap(()=> obs$),
        finalize(()=> this.loadingOff())
      )
  }

  loadingOn(){
    asapScheduler.schedule(()=> this.loading.set(true))
  }

  loadingOff(){
    asapScheduler.schedule(()=> this.loading.set(false))
  }
  
}
